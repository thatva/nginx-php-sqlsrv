FROM php:7.3.6-fpm-stretch

MAINTAINER Demian Wandelow <demian@opentierra.com>

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        git \
        libz-dev \
        libmcrypt-dev \
        libxml2-dev \
        zlib1g-dev libicu-dev g++ \
        curl libcurl4-openssl-dev \
        zlib1g-dev libgd-dev \
        file \
        unzip libpcre3 libpcre3-dev \
        freetds-bin freetds-dev freetds-common libct4 libsybdb5 tdsodbc libedit-dev libreadline6-dev libreadline-dev librecode-dev libpspell-dev

# fix for docker-php-ext-install pdo_dblib
# https://stackoverflow.com/questions/43617752/docker-php-and-freetds-cannot-find-freetds-in-know-installation-directories
RUN ln -s /usr/lib/x86_64-linux-gnu/libsybdb.so /usr/lib/

RUN docker-php-ext-configure hash --with-mhash && \
    docker-php-ext-install hash

RUN docker-php-ext-install bcmath ctype curl dba dom
RUN docker-php-ext-install fileinfo exif gettext 
RUN docker-php-ext-install intl json mbstring mysqli 
RUN docker-php-ext-install opcache pcntl 
RUN docker-php-ext-install pdo pdo_dblib pdo_mysql posix 
RUN docker-php-ext-install readline recode 
RUN docker-php-ext-install session shmop simplexml soap sockets 
RUN docker-php-ext-install sysvmsg sysvsem sysvshm 

# fix for docker-php-ext-install xmlreader
# https://github.com/docker-library/php/issues/373
RUN export CFLAGS="-I/usr/src/php" && docker-php-ext-install xmlreader xmlwriter xml xmlrpc

RUN docker-php-ext-install tokenizer 


# install MSSQL support and ODBC driver
RUN apt-get update -y && apt-get install -y apt-transport-https locales gnupg
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \
    curl https://packages.microsoft.com/config/debian/9/prod.list > /etc/apt/sources.list.d/mssql-release.list && \
    export DEBIAN_FRONTEND=noninteractive && apt-get update -y && \
    ACCEPT_EULA=Y apt-get install -y msodbcsql17 unixodbc-dev
RUN set -xe \
    && pecl install pdo_sqlsrv \
    && docker-php-ext-enable pdo_sqlsrv \
    && apt-get purge -y unixodbc-dev && apt-get autoremove -y && apt-get clean

# set locale to utf-8
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

RUN apt-get remove -y git && apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# allow all processes to write to the same log
RUN touch /tmp/stdout.log && \
    chmod u+rw,g+rw,o+rw /tmp/stdout.log

WORKDIR /app

